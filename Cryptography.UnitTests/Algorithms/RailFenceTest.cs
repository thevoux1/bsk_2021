﻿using Cryptography.Algorithms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Cryptography.UnitTests.Algorithms
{
    [TestClass]
    public class RailFenceTest
    {
        private const string SAMPLE_STRING = "SampleString";
        private const int SAMPLE_KEY = 2;

        [TestMethod]
        [Description("Should return FALSE when parameter is nonnumerical")]
        public void AreParametersValidTest1()
        {
            // Arange
            var railFence = new RailFence();

            // Act
            var result = railFence.AreParametersValid(SAMPLE_STRING);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        [Description("Should return FALSE when parameter cannot be parsed to UInt32")]
        public void AreParametersValidTest2()
        {
            // Arange
            var railFence = new RailFence();

            // Act
            var result = railFence.AreParametersValid(SAMPLE_KEY * -1);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        [Description("Should return TRUE when parameter can be parsed to UInt32")]
        public void AreParametersValidTest3()
        {
            // Arange
            var railFence = new RailFence();

            // Act
            var result = railFence.AreParametersValid(SAMPLE_KEY);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [Description("Should return inital value after encoding and decoding a value with the same key")]
        public void EncodeDecodeTest()
        {
            // Arange
            var railFence = new RailFence();

            // Act
            var encoded = railFence.Encode(SAMPLE_STRING, SAMPLE_KEY);
            var decoded = railFence.Decode(encoded, SAMPLE_KEY);

            // Assert
            Assert.AreEqual(decoded, SAMPLE_STRING);
        }

        [TestMethod]
        [Description("Should return inital value after decoding and encoding a value with the same key")]
        public void DecodeEncodeTest()
        {
            // Arange
            var railFence = new RailFence();

            // Act
            var decoded = railFence.Decode(SAMPLE_STRING, SAMPLE_KEY);
            var encoded = railFence.Encode(decoded, SAMPLE_KEY);

            // Assert
            Assert.AreEqual(encoded, SAMPLE_STRING);
        }
    }
}
