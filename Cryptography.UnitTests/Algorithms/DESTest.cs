﻿using Cryptography.Algorithms;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cryptography.UnitTests.Algorithms
{
    [TestClass]
    public class DESTest
    {
        private const string SAMPLE_STRING = "SampleString";
        private const string INVALID_KEY = "KEY";
        private const string VALID_KEY = "VALID_KEY";

        [TestMethod]
        [Description("Should return FALSE when key length is lower than 8")]
        public void AreParametersValidTest1()
        {
            // Arange
            var des = new DES();

            // Act
            var result = des.AreParametersValid(INVALID_KEY);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        [Description("Should return FALSE when key length IS NOT lower than 8")]
        public void AreParametersValidTest2()
        {
            // Arange
            var des = new DES();

            // Act
            var result = des.AreParametersValid(VALID_KEY);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [Description("Should return inital value after encoding and decoding a value with the same key")]
        public void EncodeDecodeTest()
        {
            // Arange
            var des = new DES();

            // Act
            var encoded = des.Encode(SAMPLE_STRING, VALID_KEY);
            var decoded = des.Decode(encoded, VALID_KEY);

            // Assert
            Assert.AreEqual(decoded, SAMPLE_STRING);
        }
    }
}
