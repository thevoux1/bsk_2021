﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cryptography.Algorithms;

namespace Cryptography.UnitTests.Algorithms
{
    [TestClass]
    public class DisplacementMatrixNumbersTest
    {
        private const string SAMPLE_STRING = "CRYPTOGRAPHYOSA";
        private const string SAMPLE_KEY = "3-1-4-2";

        [TestMethod]
        [Description("Should return FALSE when parameter is nonnumerical or parameter format is not properly")]
        public void AreParametersValidTest1()
        {
            // Arrange
            var matrixNumber = new DisplacementMatrixNumbers();

            // Act
            var result = matrixNumber.AreParametersValid(SAMPLE_KEY);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [Description("Should return inital value after encoding and decoding a value with the same key")]
        public void EncodeDecodeTest()
        {
            // Arrange
            var matrixNumber = new DisplacementMatrixNumbers();

            // Act
            var encoded = matrixNumber.Encode(SAMPLE_STRING, SAMPLE_KEY);
            var decoded = matrixNumber.Decode(encoded, SAMPLE_KEY);

            // Assert
            Assert.AreEqual(decoded, SAMPLE_STRING);
        }

        [TestMethod]
        [Description("Should return inital value after decoding and encoding a value with the same key")]
        public void DecodeEncodeTest()
        {
            // Arrange
            var matrixNumber = new DisplacementMatrixNumbers();

            // Act
            var decoded = matrixNumber.Decode(SAMPLE_STRING, SAMPLE_KEY);
            var encoded = matrixNumber.Encode(decoded, SAMPLE_KEY);

            // Assert
            Assert.AreEqual(encoded, SAMPLE_STRING);
        }
    }
}
