﻿using Cryptography.Algorithms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cryptography.UnitTests.Algorithms
{
    [TestClass]
    public class CaesarCipherTest
    {
        private const string SAMPLE_STRING = "SAMPLESTRING";
        private const int SAMPLE_KEY = 4;

        [TestMethod]
        [Description("Should return FALSE when parameter is nonnumerical")]
        public void AreParametersValidTest1()
        {
            // Arange
            var caesarCipher = new CaesarCipher();

            // Act
            var result = caesarCipher.AreParametersValid(SAMPLE_STRING);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        [Description("Should return FALSE when parameter cannot be parsed to UInt32")]
        public void AreParametersValidTest2()
        {
            // Arange
            var caesarCipher = new CaesarCipher();

            // Act
            var result = caesarCipher.AreParametersValid(SAMPLE_KEY * -1);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        [Description("Should return TRUE when parameter can be parsed to UInt32")]
        public void AreParametersValidTest3()
        {
            // Arange
            var caesarCipher = new CaesarCipher();

            // Act
            var result = caesarCipher.AreParametersValid(SAMPLE_KEY);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [Description("Should return inital value after encoding and decoding a value with the same key")]
        public void EncodeDecodeTest()
        {
            // Arange
            var caesarCipher = new CaesarCipher();

            // Act
            var encoded = caesarCipher.Encode(SAMPLE_STRING, SAMPLE_KEY);
            var decoded = caesarCipher.Decode(encoded, SAMPLE_KEY);

            // Assert
            Assert.AreEqual(decoded, SAMPLE_STRING);
        }

        [TestMethod]
        [Description("Should return inital value after decoding and encoding a value with the same key")]
        public void DecodeEncodeTest()
        {
            // Arange
            var caesarCipher = new CaesarCipher();

            // Act
            var decoded = caesarCipher.Decode(SAMPLE_STRING, SAMPLE_KEY);
            var encoded = caesarCipher.Encode(decoded, SAMPLE_KEY);

            // Assert
            Assert.AreEqual(encoded, SAMPLE_STRING);
        }

        [TestMethod]
        [Description("Should throw InvalidInputException when input contains banned chars")]
        public void GetEnglishCapitalLettersTest()
        {
            // Arange

            // Act

            // Assert
            Assert.ThrowsException<InvalidInputException>(() =>
            {
                CaesarCipher.GetEnglishCapitalLetters(SAMPLE_KEY.ToString(), "Wejście");
            });
        }
    }
}
