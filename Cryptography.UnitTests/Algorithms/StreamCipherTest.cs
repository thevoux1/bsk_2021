﻿using Cryptography.Algorithms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cryptography.UnitTests.Algorithms
{
    [TestClass]
    public class StreamCipherTest
    {
        private const string SAMPLE_STRING = "CRYPTOGRAPHY";
        private const string VALID_PARAMETER = "2, 4, 3";
        private const string INVALID_PARAMETER = "2, !@z, 3";

        [TestMethod]
        [Description("Should return FALSE when parameter is invalid")]
        public void AreParametersValidTest1()
        {
            // Arrange
            var streamCipher = new StreamCipher();

            // Act
            var result = streamCipher.AreParametersValid(INVALID_PARAMETER);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        [Description("Should return TRUE when parameter is valid")]
        public void AreParametersValidTest2()
        {
            // Arrange
            var streamCipher = new StreamCipher();

            // Act
            var result = streamCipher.AreParametersValid(VALID_PARAMETER);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [Description("Should return inital value after encoding and decoding a value with the same key")]
        public void EncodeDecodeTest()
        {
            // Arrange
            var streamCipher = new StreamCipher();
            StreamCipher.Key.Clear();

            // Act
            var encoded = streamCipher.Encode(SAMPLE_STRING, VALID_PARAMETER);
            var decoded = streamCipher.Decode(encoded, VALID_PARAMETER);

            // Assert
            Assert.AreEqual(decoded, SAMPLE_STRING);
        }

        [TestMethod]
        [Description("Should throw FormatException when value to decode is not binary")]
        public void DecodeEncodeTest()
        {
            // Arrange
            var streamCipher = new StreamCipher();

            // Act
            // Assert
            Assert.ThrowsException<FormatException>(() => streamCipher.Decode(SAMPLE_STRING, VALID_PARAMETER));
        }
    }
}
