﻿using Cryptography.Algorithms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cryptography.UnitTests.Algorithms
{
    [TestClass]
    public class VigenereCipherTest
    {
        private const string SAMPLE_STRING = "CRYPTOGRAPHY";
        private const string SAMPLE_KEY = "BREAKBREAKBR";

        [TestMethod]
        [Description("Should return FALSE when parameter is not a string")]
        public void AreParametersValidTest()
        {
            // Arrange
            var vigenereCipher = new VigenereCipher();

            // Act
            var result = vigenereCipher.AreParametersValid(SAMPLE_KEY);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [Description("Should return inital value after encoding and decoding a value with the same key")]
        public void EncodeDecodeTest()
        {
            // Arrange
            var vigenereCipher = new VigenereCipher();

            // Act
            var encoded = vigenereCipher.Encode(SAMPLE_STRING, SAMPLE_KEY);
            var decoded = vigenereCipher.Decode(encoded, SAMPLE_KEY);

            // Assert
            Assert.AreEqual(decoded, SAMPLE_STRING);
        }

        [TestMethod]
        [Description("Should return inital value after decoding and encoding a value with the same key")]
        public void DecodeEncodeTest()
        {
            // Arrange
            var vigenereCipher = new VigenereCipher();

            // Act
            var decoded = vigenereCipher.Decode(SAMPLE_STRING, SAMPLE_KEY);
            var encoded = vigenereCipher.Encode(decoded, SAMPLE_KEY);

            // Assert
            Assert.AreEqual(encoded, SAMPLE_STRING);
        }
    }
}