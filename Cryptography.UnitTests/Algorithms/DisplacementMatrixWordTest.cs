﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cryptography.Algorithms;

namespace Cryptography.UnitTests.Algorithms
{
    [TestClass]
    public class DisplacementMatrixWordTest
    {
        private const string SAMPLE_STRING = "HEREISASECRETMESSAGEENCIPHEREDBYTRANSPOSITION";
        private const string SAMPLE_KEY = "CONVENIENCE";

        [TestMethod]
        [Description("Should return FALSE when parameter is nonstring")]
        public void AreParametersValidTest1()
        {
            // Arrange
            var matrixWord = new DisplacementMatrixWord();

            // Act
            var result = matrixWord.AreParametersValid(SAMPLE_KEY);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        [Description("Should return inital value after encoding and decoding a value with the same key")]
        public void EncodeDecodeTest()
        {
            // Arrange
            var matrixWord = new DisplacementMatrixWord();

            // Act
            var encoded = matrixWord.Encode(SAMPLE_STRING, SAMPLE_KEY);
            var decoded = matrixWord.Decode(encoded, SAMPLE_KEY);

            // Assert
            Assert.AreEqual(decoded, SAMPLE_STRING);
        }

        [TestMethod]
        [Description("Should return inital value after decoding and encoding a value with the same key")]
        public void DecodeEncodeTest()
        {
            // Arrange
            var matrixWord = new DisplacementMatrixWord();

            // Act
            var decoded = matrixWord.Decode(SAMPLE_STRING, SAMPLE_KEY);
            var encoded = matrixWord.Encode(decoded, SAMPLE_KEY);

            // Assert
            Assert.AreEqual(encoded, SAMPLE_STRING);
        }
    }
}
