﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cryptography
{
    public class Enums
    {
        public enum AlgorithmEnum : byte
        {
            RAIL_FENCE,
            DISPLACEMENT_MATRIX_NUMBERS,
            DISPLACEMENT_MATRIX_WORD,
            DISPLACEMENT_MATRIX_WORD_TWO,
            CAESAR_CIPHER,
            VIGENERE_CIPHER,
            STREAM_CIPHER,
            DES
        }
    }
}
