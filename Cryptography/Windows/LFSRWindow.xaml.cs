﻿using Cryptography.Algorithms;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Cryptography.Windows
{
    /// <summary>
    /// Interaction logic for LFSRWindow.xaml
    /// </summary>
    public partial class LFSRWindow : Window
    {
        private bool _isGenerating = false;
        LFSRGenerator lfsrGenerator;
        public LFSRWindow()
        {
            InitializeComponent();
        }

        private void StartGenerating_BT_Click(object sender, RoutedEventArgs e)
        {
            List<uint> powers = LFSRGenerator.GetPowers(MaxPower_TB.Text);
            if(powers == null)
            {
                MessageBox.Show("Nieprawidłowy format potęg", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            lfsrGenerator = new LFSRGenerator(powers);
            MaxPowerInfo_LB.Content = "Wygenerowany ciąg pseudolosowy dla potęgi " + MaxPower_TB.Text + ":";
            Result_TB.Text = "";
            ContinueGenerating_BT_Click(sender, e);
        }

        private async void ContinueGenerating_BT_Click(object sender, RoutedEventArgs e)
        {
            StartGenerating_BT.IsEnabled = false;
            ContinueGenerating_BT.IsEnabled = false;
            PauseGenerating_BT.IsEnabled = true;
            _isGenerating = true;

            await Task.Run(() =>
            {
                while (_isGenerating)
                {
                    int generated = lfsrGenerator.GenerateNext();
                    if(Application.Current != null)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            Result_TB.Text += generated;
                            Result_TB.ScrollToEnd();
                        });
                    }
                    Thread.Sleep(5);
                }
            });
        }

        private void PauseGenerating_BT_Click(object sender, RoutedEventArgs e)
        {
            _isGenerating = false;
            StartGenerating_BT.IsEnabled = true;
            ContinueGenerating_BT.IsEnabled = true;
            PauseGenerating_BT.IsEnabled = false;
        }

        private void ReadKey_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            if (openFileDialog.ShowDialog() == true)
            {
                string key = File.ReadAllText(openFileDialog.FileName).Trim();
                if(key.Length == 0 || key.Count(x => x == '0' || x == '1') != key.Length)
                {
                    MessageBox.Show("Nieprawidłowy format klucza!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                Result_TB.Text = key;
                Result_TB.ScrollToEnd();
            }
        }

        private void ApplyKey_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if(Result_TB.Text.Trim() == "")
            {
                MessageBox.Show("Brak klucza do zastosowania!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            PauseGenerating_BT_Click(sender, e);
            StreamCipher.SetKey(Result_TB.Text);
            MessageBox.Show("Pomyślnie zastosowano klucz", "Operacja zakończona powodzeniem", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
