﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Cryptography
{
    /// <summary>
    /// Interaction logic for FileComparisonWindow.xaml
    /// </summary>
    public partial class FileComparisonWindow : Window
    {
        private string _firstFilePath;
        private string _secondFilePath;

        public FileComparisonWindow()
        {
            InitializeComponent();
        }

        private void SelectFirstFile_BT_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            if (openFileDialog.ShowDialog() == true)
            {
                _firstFilePath = openFileDialog.FileName;
                FirstFilePath_LB.Content = _firstFilePath;
            }
        }

        private void SelectSecondFile_BT_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            if (openFileDialog.ShowDialog() == true)
            {
                _secondFilePath = openFileDialog.FileName;
                SecondFilePath_LB.Content = _secondFilePath;
            }
        }

        private void Compare_BT_Click(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(_firstFilePath))
            {
                MessageBox.Show("Pierwszy plik nie istnieje", "Błędny plik", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (!File.Exists(_secondFilePath))
            {
                MessageBox.Show("Drugi plik nie istnieje", "Błędny plik", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if(_firstFilePath == _secondFilePath)
            {
                MessageBox.Show("Wybrano dwa razy ten sam plik", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            List<string> firstFileLines = File.ReadAllLines(_firstFilePath).ToList();
            List<string> secondFileLines = File.ReadAllLines(_secondFilePath).ToList();

            //usuwanie pustych linii z końca pierwszego pliku
            for (int i=firstFileLines.Count - 1; i >= 0; i--)
            {
                if (firstFileLines[i] == "")
                    firstFileLines.RemoveAt(i);
            }

            //usuwanie pustych linii z końca drugiego pliku
            for (int i = secondFileLines.Count - 1; i >= 0; i--)
            {
                if (secondFileLines[i] == "")
                    secondFileLines.RemoveAt(i);
            }

            if(secondFileLines.Count != firstFileLines.Count)
            {
                Result_LB.Foreground = new SolidColorBrush(Colors.Red);
                Result_LB.Content = "Niezgodne (różna liczba linii)";
                return;
            }

            for(int i = 0; i < firstFileLines.Count; i++)
            {
                if(firstFileLines[i].ToUpper() != secondFileLines[i].ToUpper())
                {
                    Result_LB.Foreground = new SolidColorBrush(Colors.Red);
                    Result_LB.Content = "Niezgodne (linia " + (i + 1) + ")";
                    return;
                }
            }

            Result_LB.Foreground = new SolidColorBrush(Colors.Green);
            Result_LB.Content = "Zgodne";
        }
    }
}
