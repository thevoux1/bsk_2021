﻿using Cryptography.Algorithms;
using Cryptography.Windows;
using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static Cryptography.Enums;

namespace Cryptography
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string _sourceFilePath;
        private string _destinationFileLocation;

        public MainWindow()
        {
            InitializeComponent();
            Algorithm_CB.ItemsSource = GetAllAlgorithms();
            MaxHeight = Height;
            MinHeight = Height;
            MinWidth = Width;
        }

        /// <summary>
        /// Zwraca listę wszystkich algorytmów, gdzie index algorytmu w liście to index z enuma Algorithm
        /// </summary>
        public List<string> GetAllAlgorithms()
        {
            List<string> result = new List<string>();
            result.Add("Rail fence");
            result.Add("Przestawienia macierzowe (liczby)");
            result.Add("Przestawienia macierzowe (słowo)");
            result.Add("Przestawienia macierzowe (słowo) v2");
            result.Add("Szyfr Cezara");
            result.Add("Szyfr Vigenere'a");
            result.Add("Szyfr strumieniowy");
            result.Add("DES");
            return result;
        }

        private void SelectSourceFile_BT_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            if (openFileDialog.ShowDialog() == true)
            {
                _sourceFilePath = openFileDialog.FileName;
                SoruceFilePath_LB.Content = _sourceFilePath;
                Extension_LB.Content = System.IO.Path.GetExtension(_sourceFilePath);
            }
        }

        private void SelectDestinationFileLocation_BT_Click(object sender, RoutedEventArgs e)
        {
            CommonOpenFileDialog commonOpenFileDialog = new CommonOpenFileDialog();
            commonOpenFileDialog.IsFolderPicker = true;

            if (commonOpenFileDialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                _destinationFileLocation = commonOpenFileDialog.FileName;
                DestinationFileLocation_RB.Content = _destinationFileLocation;
            }
        }

        private void KeyboardStart_BT_Click(object sender, RoutedEventArgs e)
        {
            AlgorithmEnum algorithmEnum = (AlgorithmEnum)Algorithm_CB.SelectedIndex;
            string input = Input_TB.Text.Trim();
            bool isEncoding = (bool)Encrypt_RB.IsChecked;

            if (algorithmEnum == AlgorithmEnum.STREAM_CIPHER)
            {
                if(Key_TB.Text.Trim() == "" && FirstParameterValue_TB.Text.Trim() == "")
                {
                    MessageBox.Show("Nie podano klucza ani parametru!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                if (Key_TB.Text.Trim() != "")
                {
                    string key = Key_TB.Text.Trim();
                    if (key.Count(x => x == '0' || x == '1') != key.Length)
                    {
                        MessageBox.Show("Nieprawidłowy format klucza!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }

                if(!isEncoding && (input.Length % 8 != 0 || input.Count(x => x == '0' || x == '1') != input.Length))
                {
                    MessageBox.Show("Nieprawidłowy format wejścia!", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            StartCryptography(algorithmEnum, isEncoding, false);
        }

        private void FileStart_BT_Click(object sender, RoutedEventArgs e)
        {
            if(!File.Exists(_sourceFilePath))
            {
                MessageBox.Show("Plik źródłowy nie istnieje", "Błędny plik źródłowy", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if(DestinationFileName_RB.IsChecked == true)
            {
                string fileName = DestinationFileName_TB.Text;

                if (fileName.Trim() == "")
                {
                    MessageBox.Show("Nazwa pliku docelowego nie może być pusta", "Błędna nazwa pliku", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                if (fileName.IndexOfAny(new char[] { '\\', '/', ':', '*', '?', '"', '<', '>', '|' }) != -1)
                {
                    MessageBox.Show("Nazwa pliku docelowego zawiera niedozwolone znaki:\n\\ / : * ? \" < > |", "Błędna nazwa pliku", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            if(DestinationFileLocation_RB.IsChecked == true
                && !Directory.Exists(_destinationFileLocation))
            {
                MessageBox.Show("Lokalizacja pliku docelowego nie istnieje", "Błędny lokalizacja pliku", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            AlgorithmEnum algorithmEnum = (AlgorithmEnum)Algorithm_CB.SelectedIndex;
            bool isEncoding = (bool)Encrypt_RB.IsChecked;
            StartCryptography(algorithmEnum, isEncoding, true);
        }

        private async void StartCryptography(AlgorithmEnum algorithmEnum, bool isEncoding, bool isFile)
        {
            IAlgorithm algorithm = null;
            string content = Input_TB.Text;
            string parameter = FirstParameterValue_TB.Text;

            switch (algorithmEnum)
            {
                case AlgorithmEnum.RAIL_FENCE:
                    algorithm = new RailFence();
                    break;
                case AlgorithmEnum.DISPLACEMENT_MATRIX_NUMBERS:
                    algorithm = new DisplacementMatrixNumbers();
                    break;
                case AlgorithmEnum.DISPLACEMENT_MATRIX_WORD:
                    algorithm = new DisplacementMatrixWord();
                    break;
                case AlgorithmEnum.DISPLACEMENT_MATRIX_WORD_TWO:
                    algorithm = new DisplacementMatrixWordTwo();
                    break;
                case AlgorithmEnum.CAESAR_CIPHER:
                    algorithm = new CaesarCipher();
                    break;
                case AlgorithmEnum.VIGENERE_CIPHER:
                    algorithm = new VigenereCipher();
                    break;
                case AlgorithmEnum.STREAM_CIPHER:
                    algorithm = new StreamCipher();
                    break;
                case AlgorithmEnum.DES:
                    algorithm = new DES();
                    break;
            }

            if (algorithm is StreamCipher)
            {
                StreamCipher.Index = 0;
                if (isEncoding && FirstParameterValue_TB.Text.Trim() != "")
                    StreamCipher.Key.Clear();

                if (Key_TB.Text.Trim() != "" && FirstParameterValue_TB.Text.Trim() == "")
                    StreamCipher.SetKey(Key_TB.Text.Trim());
            }

            if ((!(algorithm is StreamCipher) || algorithm is StreamCipher && StreamCipher.Key.Count < 1) && !algorithm.AreParametersValid(parameter))
            {
                MessageBox.Show("Podany parametr jest błędny lub brak klucza", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                Output_TB.Text = "";
                return;
            }

            int startTick = Environment.TickCount;
            Footer_PB.Visibility = Visibility.Visible;
            Footer_LB.Content = "Przetwarzanie...";
            KeyboardStart_BT.IsEnabled = false;
            FileStart_BT.IsEnabled = false;

            try
            {
                string extension = System.IO.Path.GetExtension(_sourceFilePath);

                if (isFile && (!(algorithm is IAlgorithmBinary) || algorithm is IAlgorithmBinary && extension != ".txt"))
                {
                    string[] result = null;
                    byte[] bytesResult = null;

                    if (isEncoding)
                    {
                        await Task.Run(() =>
                        {
                            if(extension == ".txt")
                            {
                                result = EncodeFile(algorithm, _sourceFilePath, parameter);
                            }
                            else
                            {
                                bytesResult = EncodeBFile((IAlgorithmBinary)algorithm, _sourceFilePath, parameter);
                            }
                        });
                    }
                    else
                    {
                        await Task.Run(() =>
                        {
                            if (extension == ".txt")
                            {
                                result = DecodeFile(algorithm, _sourceFilePath, parameter);
                            } 
                            else
                            {
                                bytesResult = DecodeBFile((IAlgorithmBinary)algorithm, _sourceFilePath, parameter);
                            }
                        });
                    }

                    string destinationPath = GetDestinationPath();
                    if(result != null || bytesResult != null)
                    {
                        if (extension == ".txt")
                        {
                            File.WriteAllLines(destinationPath, result);
                        }
                        else
                        {
                            using (FileStream fs = new FileStream(destinationPath, FileMode.CreateNew))
                            {
                                using (BinaryWriter bw = new BinaryWriter(fs))
                                {
                                    bw.Write(bytesResult);
                                    bw.Close();
                                }
                                fs.Close();
                            }
                        }
                    }

                    if(algorithm is StreamCipher && isEncoding)
                    {
                        string keyPath = GetDestinationPath(true);
                        File.WriteAllText(keyPath, string.Join("", StreamCipher.Key.Select(x => x ? "1" : "0")));
                    }
                }
                else
                {
                    if (algorithm is StreamCipher && isFile)
                        content = File.ReadAllText(_sourceFilePath);

                    string result = null;

                    if (isEncoding)
                    {
                        await Task.Run(() =>
                        {
                            result = algorithm.Encode(content, parameter);
                        });
                    }
                    else
                    {
                        await Task.Run(() =>
                        {
                            result = algorithm.Decode(content, parameter);
                        });
                    }

                    if(algorithm is StreamCipher && isFile)
                    {
                        string destinationPath = GetDestinationPath();
                        File.WriteAllText(destinationPath, result);

                        if(isEncoding)
                        {
                            string keyPath = GetDestinationPath(true);
                            File.WriteAllText(keyPath, string.Join("", StreamCipher.Key.Select(x => x ? "1" : "0")));
                        }
                    }
                    else
                    {
                        Output_TB.Text = result;
                        Key_TB.Text = string.Join("", StreamCipher.Key.Select(x => x ? "1" : "0"));
                    }

                }
            }
            catch(Exception ex)
            {
                Footer_PB.Visibility = Visibility.Collapsed;
                KeyboardStart_BT.IsEnabled = true;
                FileStart_BT.IsEnabled = true;
                Footer_LB.Content = "";
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            int stopTick = Environment.TickCount;
            Footer_PB.Visibility = Visibility.Collapsed;
            KeyboardStart_BT.IsEnabled = true;
            FileStart_BT.IsEnabled = true;
            Footer_LB.Content = String.Format("Czas wykonania: {0}ms", stopTick - startTick);
        }

        private void Algorithm_CB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AlgorithmEnum algorithm = (AlgorithmEnum)Algorithm_CB.SelectedIndex;

            switch (algorithm)
            {
                case AlgorithmEnum.RAIL_FENCE:
                    FirstParameterName_LB.Content = "n = ";
                    break;
                case AlgorithmEnum.DISPLACEMENT_MATRIX_NUMBERS:
                case AlgorithmEnum.DISPLACEMENT_MATRIX_WORD:
                case AlgorithmEnum.DISPLACEMENT_MATRIX_WORD_TWO:
                case AlgorithmEnum.VIGENERE_CIPHER:
                case AlgorithmEnum.DES:
                    FirstParameterName_LB.Content = "key = ";
                    break;
                case AlgorithmEnum.CAESAR_CIPHER:
                    FirstParameterName_LB.Content = "k = ";
                    break;
                case AlgorithmEnum.STREAM_CIPHER:
                    FirstParameterName_LB.Content = "Potęgi wielomianu = ";
                    break;
            }

            if (algorithm != AlgorithmEnum.STREAM_CIPHER)
            {
                Key_LB.Visibility = Visibility.Collapsed;
                Key_TB.Visibility = Visibility.Collapsed;
            }
            else
            {
                Key_LB.Visibility = Visibility.Visible;
                Key_TB.Visibility = Visibility.Visible;
            }
        }

        private string[] EncodeFile(IAlgorithm algorithm, string filePath, object parameters)
        {
            string[] content = File.ReadAllLines(filePath);
            string[] result = new string[content.Length];

            for(int i=0; i<content.Length; i++)
                result[i] = algorithm.Encode(content[i], parameters);

            return result;
        }
        
        private byte[] EncodeBFile(IAlgorithmBinary algorithm, string filePath, object parameters)
        {
            byte[] bytes;
            using (FileStream fs = new FileStream(filePath, FileMode.Open))
            {
                bytes = new byte[fs.Length];
                int i = 0;
                using (BinaryReader br = new BinaryReader(fs))
                {
                    while( i < fs.Length)
                    {
                        bytes[i++] = br.ReadByte();
                    }
                    
                    br.Close();
                }
                fs.Close();
            }

            byte[] res;

            if (algorithm is StreamCipher)
            {
                res = new byte[bytes.Length];

                while (StreamCipher.Key.Count < bytes.Length * 4)
                    StreamCipher.Key.AddRange(StreamCipher.Key);

                if (StreamCipher.Key.Count > bytes.Length * 4)
                    StreamCipher.Key.RemoveRange(bytes.Length * 4, StreamCipher.Key.Count - bytes.Length * 4);

                for (int i = 0; i < bytes.Length; i++)
                    res[i] = algorithm.EncodeByte(bytes[i], parameters);
            }
            else // DES
            {
                var des = algorithm as DES;
                res = des.Encode(bytes, Encoding.UTF8.GetBytes(parameters.ToString()));
            }

            return res;
        }

        private string[] DecodeFile(IAlgorithm algorithm, string filePath, object parameters)
        {
            string[] content = File.ReadAllLines(filePath);
            string[] result = new string[content.Length];

            for (int i = 0; i < content.Length; i++)
                result[i] = algorithm.Decode(content[i], parameters);

            return result;
        }

        private byte[] DecodeBFile(IAlgorithmBinary algorithm, string filePath, object parameters)
        {
            byte[] bytes;
            using (FileStream fs = new FileStream(filePath, FileMode.Open))
            {
                bytes = new byte[fs.Length];
                int i = 0;
                using (BinaryReader br = new BinaryReader(fs))
                {
                    while (i < fs.Length)
                    {
                        bytes[i++] = br.ReadByte();
                    }

                    br.Close();
                }
                fs.Close();
            }

            byte[] res;

            if (algorithm is StreamCipher)
            {
                res = new byte[bytes.Length];

                while (StreamCipher.Key.Count < bytes.Length * 4)
                    StreamCipher.Key.AddRange(StreamCipher.Key);

                if (StreamCipher.Key.Count > bytes.Length * 4)
                    StreamCipher.Key.RemoveRange(bytes.Length * 4, StreamCipher.Key.Count - bytes.Length * 4);

                for (int i = 0; i < bytes.Length; i++)
                    res[i] = algorithm.DecodeByte(bytes[i], parameters);
            }
            else // DES
            {
                var des = algorithm as DES;
                res = des.Decode(bytes, Encoding.UTF8.GetBytes(parameters.ToString()));
            }

            return res;
        }

        private string GetDestinationPath(bool getKey = false)
        {
            string path;

            if (DestinationFileLocation_RB.IsChecked == true)
                path = _destinationFileLocation;
            else
                path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            path += "\\";

            if (DestinationFileName_RB.IsChecked == true)
            {
                path += DestinationFileName_TB.Text;
                if (getKey)
                    path += "_key";
            }
            else
            {
                path += ((AlgorithmEnum)Algorithm_CB.SelectedIndex).ToString();

                if (getKey)
                    path += "_key_";
                else if (Encrypt_RB.IsChecked == true)
                    path += "_encode_";
                else
                    path += "_decode_";

                path += DateTime.Now.ToString("dd-MM-yy_HH-mm-ss");
            }

            if (getKey)
                path += ".txt";
            else
                path += System.IO.Path.GetExtension(_sourceFilePath);

            return path;
        }

        private void FileComparison_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            FileComparisonWindow fileComparisonWindow = new FileComparisonWindow();
            fileComparisonWindow.Owner = this;
            fileComparisonWindow.ShowDialog();
        }

        private void Authors_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Piotr Wyszyński (105802)\nSzymon Karolczuk (105567)\nPrzemysław Stupak (105759)", "Autorzy", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void DestinationFileName_TB_GotFocus(object sender, RoutedEventArgs e)
        {
            DestinationFileName_RB.IsChecked = true;
        }

        private void LSFR_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            LFSRWindow lfsrWindow = new LFSRWindow();
            lfsrWindow.Owner = this;
            lfsrWindow.ShowDialog();
        }
    }
}
