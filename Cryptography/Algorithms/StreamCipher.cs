﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cryptography.Algorithms
{
    public class StreamCipher : IAlgorithm, IAlgorithmBinary
    {
        public static LFSRGenerator LfsrGenerator;
        public static List<bool> Key = new List<bool>();
        public static int Index = 0;

        public bool AreParametersValid(object parameters)
        {
            return LFSRGenerator.GetPowers(parameters.ToString()) != null;
        }

        public string Decode(string content, object parameters)
        {
            if(Key.Count < 1)
            {
                List<uint> powers = LFSRGenerator.GetPowers(parameters.ToString());
                LfsrGenerator = new LFSRGenerator(powers);

                while (Key.Count < content.Length)
                    Key.Add(LfsrGenerator.GenerateNext() > 0);
            }

            string result = String.Empty;
            int resultInInt, bit;
            bool resultInBoolean;
            for (int i = 0; i < content.Length; i++)
            {
                bit = int.Parse(content[i].ToString());
                resultInBoolean = LfsrGenerator.XOR(bit, Key[Index++] ? 1 : 0);
                resultInInt = resultInBoolean == true ? 1 : 0;
                result += resultInInt.ToString();
            }
            List<Byte> byteList = new List<Byte>();
            for (int i = 0; i < result.Length; i += 8)
            {
                byteList.Add(Convert.ToByte(result.Substring(i, 8), 2));
            }
            return Encoding.ASCII.GetString(byteList.ToArray());
        }

        public string Encode(string content, object parameters)
        {
            StringBuilder sb = new StringBuilder();
            foreach(char c in content) // konwersja do postaci binarnej
                sb.Append(Convert.ToString(c, 2).PadLeft(8, '0'));

            content = sb.ToString();
            if (parameters.ToString().Trim() != "")
            {
                List<uint> powers = LFSRGenerator.GetPowers(parameters.ToString());
                LfsrGenerator = new LFSRGenerator(powers);
            }
            else
            {
                while (Key.Count < content.Length)
                    Key.AddRange(Key);

                if (Key.Count > content.Length)
                    Key.RemoveRange(content.Length, Key.Count - content.Length);
            }

            int lfsrGeneratedBit, bit, resultInInt;
            bool resultInBoolean;
            string result = String.Empty;

            for(int i=0; i<content.Length; i++)
            {
                bit = int.Parse(content[i].ToString());

                if (parameters.ToString().Trim() != "")
                {
                    lfsrGeneratedBit = LfsrGenerator.GenerateNext();
                    Key.Add(lfsrGeneratedBit > 0);
                }
                else
                {
                    lfsrGeneratedBit = Key[i] ? 1 : 0;
                }

                resultInBoolean = LfsrGenerator.XOR(bit, lfsrGeneratedBit);
                resultInInt = resultInBoolean == true ? 1 : 0;
                result += resultInInt.ToString();
            }
            return result;
        }

        public byte EncodeByte(byte content, object parameters)
        {
            int lfsrGeneratedBit;
            byte gen = 0x00;

            if (parameters.ToString().Trim() != "")
            {
                List<uint> powers = LFSRGenerator.GetPowers(parameters.ToString());
                LfsrGenerator = new LFSRGenerator(powers);

                for (int i = 0; i < 4; i++)
                {
                    lfsrGeneratedBit = LfsrGenerator.GenerateNext();
                    Key.Add(lfsrGeneratedBit > 0);
                    gen += (byte)lfsrGeneratedBit;
                }
            }
            else
            {
                for (int i = 0; i < 4; i++)
                {
                    lfsrGeneratedBit = Key[Index++] ? 1 : 0;
                    gen += (byte)lfsrGeneratedBit;
                }
            }

            byte res = (byte)(gen ^ content);

            return res;
        }

        public byte DecodeByte(byte content, object parameters)
        {
            int lfsrGeneratedBit;
            byte gen = 0x00;
            lfsrGeneratedBit = Key[Index++] ? 1 : 0;
            gen += (byte)lfsrGeneratedBit;
            lfsrGeneratedBit = Key[Index++] ? 1 : 0;
            gen += (byte)lfsrGeneratedBit;
            lfsrGeneratedBit = Key[Index++] ? 1 : 0;
            gen += (byte)lfsrGeneratedBit;
            lfsrGeneratedBit = Key[Index++] ? 1 : 0;
            gen += (byte)lfsrGeneratedBit;

            byte res = (byte)(gen ^ content);

            return res;
        }

        public static void SetKey(string key)
        {
            Key.Clear();
            foreach (char c in key)
            {
                if (c == '0')
                    Key.Add(false);
                else
                    Key.Add(true);
            }
        }
    }
}
