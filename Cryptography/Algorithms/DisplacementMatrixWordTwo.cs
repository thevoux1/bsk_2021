﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Cryptography.Algorithms
{
    public class DisplacementMatrixWordTwo : IAlgorithm
    {
        public bool AreParametersValid(object parameters)
        {
            return parameters is String && ((String)parameters).Trim().Length > 0;
        }

        public string Decode(string content, object parameters)
        {
            string key = (parameters as String).Trim().ToUpper();
            content = Regex.Replace(content, @"\s+", ""); // usuwa spacje
            content = content.ToUpper();

            // poprawka dla wyrazów krótszych niż klucz
            if (key.Length > content.Length) 
            {
                key = key.Substring(0, content.Length);
            }
            // =============

            List<KeyValuePair<char, int>> keyList = new List<KeyValuePair<char, int>>();

            for (int i = 0; i < key.Length; i++)
            {
                keyList.Add(new KeyValuePair<char, int>(key[i], i));
            }
            keyList = keyList.OrderBy(x => x.Key).ToList();

            int columns = key.Length;
            int rows = key.Length;

            char[,] matrix = new char[rows, columns];

            // przygotowanie macierzy
            int r = 0, c = 0;
            for (int i = 0; i < content.Length; i++)
            {
                matrix[r, c++] = '\x18';
                if (c > keyList[r].Value)
                {
                    r++;
                    c = 0;
                }
            }

            // uzupełnienie macierzy
            int z = 0;
            r = 0; c = 0;
            while (z < content.Length)
            {
                if(matrix[r, keyList[c].Value] == '\x18')
                {
                    matrix[r, keyList[c].Value] = content[z++];
                }
                r++;
                if(r >= rows)
                {
                    c++;
                    r = 0;
                }
            }

            string result = "";
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    if (matrix[i, j] != '\x0000')
                        result += matrix[i, j];
                }
            }

            return result;
        }

        public string Encode(string content, object parameters)
        {
            string key = (parameters as String).Trim().ToUpper();
            content = content.ToUpper();
            content = Regex.Replace(content, @"\s+", ""); // usuwa spacje

            // poprawka dla wyrazów krótszych niż klucz
            if (key.Length > content.Length)
            {
                key = key.Substring(0, content.Length);
            }
            // =============

            List<KeyValuePair<char, int>> keyList = new List<KeyValuePair<char, int>>();

            for (int i = 0; i < key.Length; i++)
            {
                keyList.Add( new KeyValuePair<char, int>(key[i], i) );
            }
            keyList = keyList.OrderBy(x => x.Key).ToList();

            int columns = key.Length;
            int rows = key.Length;

            char[,] matrix = new char[rows, columns];

            int r = 0, c = 0;
            for (int i = 0; i < content.Length; i++)
            {
                matrix[r, c++] = content[i];
                if (c > keyList[r].Value)
                {
                    r++;
                    c = 0;
                }
            }

            // budowa wyniku
            string result = "";
            int column = 0;
            for (int i = 0; i < columns; i++)
            {
                column = keyList[i].Value;
                for(int j = 0; j < rows; j++)
                {
                    if (matrix[j, column] != '\x0000')
                        result += matrix[j, column];
                }
            }

            return result;
        }
    }
}
