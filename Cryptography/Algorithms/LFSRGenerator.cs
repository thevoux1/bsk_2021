﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cryptography.Algorithms
{
    public class LFSRGenerator
    {
        bool[] qBlocks;
        List<uint> powers;

        public LFSRGenerator(List<uint> input)
        {
            powers = input;
            qBlocks = new bool[input.Max() + 1];

            // inicjalizacja
            Random random = new Random();
            for (int i = 0; i < qBlocks.Length; i++)
                qBlocks[i] = random.Next(2) > 0;
        }

        public int GenerateNext()
        {
            int next = qBlocks[qBlocks.Length - 1] ? 1 : 0;
            bool xorResult = qBlocks[powers[0]];

            for (int i = 1; i < powers.Count; i++)
                xorResult = xorResult ^ qBlocks[powers[i]];

            for (int i = qBlocks.Length - 1; i > 0; i--)
                qBlocks[i] = qBlocks[i - 1];

            qBlocks[0] = xorResult;
            return next;
        }

        public bool XOR(int x1, int y1)
        {
            bool x, y;
            y = y1 == 1 ? true : false;
            x = x1 == 1 ? true : false;
            return x ^ y;
        }

        public static List<uint> GetPowers(string input)
        {
            if (input.Length == 0)
                return null;

            List<uint> result = new List<uint>();
            string[] values = input.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            foreach(string value in values)
            {
                if (!UInt32.TryParse(value, out uint power))
                    return null;

                if (result.Contains(power))
                    return null;

                result.Add(power);
            }

            return result;
        }
    }
}
