﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Cryptography.Algorithms
{
    public class DisplacementMatrixWord : IAlgorithm
    {
        public bool AreParametersValid(object parameters)
        {
            return parameters is String && ((String)parameters).Length > 0;
        }

        public string Decode(string content, object parameters)
        {
            string key = (parameters as String).ToUpper();
            content = Regex.Replace(content, @"\s+", ""); // usuwa spacje
            content = content.ToUpper();

            // poprawka dla wyrazów krótszych niż klucz
            if (key.Length > content.Length) 
            {
                key = key.Substring(0, content.Length);
            }
            // =============

            List<KeyValuePair<char, int>> keyList = new List<KeyValuePair<char, int>>();

            for (int i = 0; i < key.Length; i++)
            {
                keyList.Add(new KeyValuePair<char, int>(key[i], i));
            }
            keyList = keyList.OrderBy(x => x.Key).ToList();

            int columns = key.Length;
            int rows = content.Length / columns + 1;
            int inLastRow = content.Length % key.Length;

            char[,] matrix = new char[rows, columns];

            int r = 0, c = 0;
            for (int i = 0; i < content.Length; i++)
            {
                if (r >= rows || (keyList[c].Value >= inLastRow && r >= rows - 1))
                {
                    c++;
                    r = 0;
                }
                matrix[r++, keyList[c].Value] = content[i];
            }

            string result = "";
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    if (matrix[i, j] != '\x0000')
                        result += matrix[i, j];
                }
            }

            return result;
        }

        public string Encode(string content, object parameters)
        {
            string key = (parameters as String).ToUpper();
            content = content.ToUpper();
            content = Regex.Replace(content, @"\s+", ""); // usuwa spacje

            // poprawka dla wyrazów krótszych niż klucz
            if (key.Length > content.Length)
            {
                key = key.Substring(0, content.Length);
            }
            // =============

            List<KeyValuePair<char, int>> keyList = new List<KeyValuePair<char, int>>();

            for (int i = 0; i < key.Length; i++)
            {
                keyList.Add( new KeyValuePair<char, int>(key[i], i) );
            }
            keyList = keyList.OrderBy(x => x.Key).ToList();

            int columns = key.Length;
            int rows = content.Length / columns + 1;

            char[,] matrix = new char[rows, columns];

            int r = 0, c = 0;
            for (int i = 0; i < content.Length; i++)
            {
                matrix[r, c++] = content[i];
                if (c >= columns)
                {
                    r++;
                    c = 0;
                }
            }

            // budowa wyniku
            string result = "";
            int column = 0;
            for (int i = 0; i < columns; i++)
            {
                column = keyList[i].Value;
                for(int j = 0; j < rows; j++)
                {
                    if (matrix[j, column] != '\x0000')
                        result += matrix[j, column];
                }
            }

            return result;
        }
    }
}
