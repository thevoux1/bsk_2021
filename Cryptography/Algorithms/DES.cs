﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cryptography.Algorithms
{
    public class DES : IAlgorithm, IAlgorithmBinary
    {
        private static int[] Step6Shifts = { 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1 };

        private static int[] S1 =
        {
            14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
            0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
            4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
            15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13
        };
        private static int[] S2 =
        {
            15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
            3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
            0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
            13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9
        };
        private static int[] S3 =
        {
            10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
            13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
            13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
            1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12,
        };
        private static int[] S4 =
        {
            7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
            13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
            10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
            3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14,
        };
        private static int[] S5 =
        {
            2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
            14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
            4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
            11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3
        };
        private static int[] S6 =
        {
            12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
            10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
            9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6,
            4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13
        };
        private static int[] S7 =
        {
            4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
            13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
            1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
            6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12
        };
        private static int[] S8 =
        {
            13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
            1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
            7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
            2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11
        };

        private static int[][] Step12Arrays = { S1, S2, S3, S4, S5, S6, S6, S7, S8 };

        public bool AreParametersValid(object parameters)
        {
            if (parameters.ToString().Length < 8)
                return false;

            return true;
        }

        public string Decode(string content, object parameters)
        {
            if (content.Length == 0)
                return "";

            byte[] bytes;

            try
            {
                bytes = System.Convert.FromBase64String(content);
            }
            catch
            {
                throw new Exception("Błędne dane wejściowe");
            }

            byte[] key = Encoding.UTF8.GetBytes(parameters.ToString());

            bytes = Decode(bytes, key);
            return Encoding.UTF8.GetString(bytes);
        }

        public string Encode(string content, object parameters)
        {
            if (content.Length == 0)
                return "";

            byte[] bytes = Encoding.UTF8.GetBytes(content);

            byte[] key = Encoding.UTF8.GetBytes(parameters.ToString());

            bytes = Encode(bytes, key);
            return System.Convert.ToBase64String(bytes);
        }

        public byte EncodeByte(byte content, object parameters)
        {
            byte[] key = Encoding.UTF8.GetBytes(parameters.ToString());

            byte[] bytes = { content };

            bytes = Encode(bytes, key);

            return bytes[0];
        }

        public byte DecodeByte(byte content, object parameters)
        {
            byte[] key = Encoding.UTF8.GetBytes(parameters.ToString());

            byte[] bytes = { content };

            bytes = Decode(bytes, key);

            return bytes[0];
        }

        public byte[] Decode(byte[] bytes, byte[] mkey)
        {
            // krok 1
            bytes = ComplementBytes(bytes);
            BitArray[] bits = CreateTwoDimensionArray(new BitArray(bytes));
            int n = bits.GetUpperBound(0);

            for (int i = 0; i <= n; i++)
            {
                // krok 2
                bits[i] = InitialPermutation(bits[i]);
                // krok 3
                BitArray[] parts = DivideBitsInHalf(bits[i]);
                BitArray leftPart = parts[0]; //Ln
                BitArray rightPart = parts[1]; //Rn
                BitArray[] key = new BitArray[16];
                for (int j = 1; j <= 16; j++)
                {
                    // krok 4
                    key[16 - j] = PermutedChoice(new BitArray(mkey));
                    // krok 5
                    BitArray[] keyParts = DivideBitsInHalf(key[16 - j]);
                    BitArray keyLeftPart = keyParts[0];
                    BitArray keyRightPart = keyParts[1];
                    // krok 6
                    keyLeftPart = ShiftLeft(keyLeftPart, Step6Shifts[i%16]); //Cn
                    keyRightPart = ShiftLeft(keyRightPart, Step6Shifts[i%16]); //Dn
                    // krok 7
                    key[16 - j] = PermutedChoice2(SumBlocks(keyLeftPart, keyRightPart));
                    // krok 8
                    BitArray rightExtend = ExtendTablePermutation(rightPart);
                    // krok 9
                    BitArray afterXOR = rightExtend.Xor(key[16 - j]);
                    // krok 10
                    BitArray[] bit6Parts = DivideBySixBits(afterXOR);
                    // krok 12
                    BitArray[] bit6Parts4Bits = Step12Function(bit6Parts);
                    // krok 13
                    BitArray mergedArray = MergeArray(bit6Parts4Bits);
                    // krok 14
                    BitArray permutedMergedArray = Step14(mergedArray);
                    // krok 15
                    BitArray rightPartPrevious = rightPart;
                    rightPart = permutedMergedArray.Xor(leftPart);
                    // krok 16
                    leftPart = rightPartPrevious;
                }

                // krok 17,18
                bits[i] = InverseInitialPermutation(SumBlocks(rightPart, leftPart));
            }

            bytes = BitArrayToByteArray(bits);
            return DecomplementBytes(bytes);
        }

        public byte[] Encode(byte[] bytes, byte[] mkey)
        {
            // krok 1
            bytes = ComplementBytes(bytes);
            BitArray[] bits = CreateTwoDimensionArray(new BitArray(bytes));
            int n = bits.Length;

            for (int i = 0; i < n; i++)
            {
                // krok 2
                bits[i] = InitialPermutation(bits[i]);
                // krok 3
                BitArray[] parts = DivideBitsInHalf(bits[i]);
                BitArray leftPart = parts[0]; //Ln
                BitArray rightPart = parts[1]; //Rn
                BitArray[] key = new BitArray[16];
                for(int j = 1; j <= 16; j++)
                {
                    // krok 4
                    key[j-1] = PermutedChoice(new BitArray(mkey));
                    // krok 5
                    BitArray[] keyParts = DivideBitsInHalf(key[j-1]);
                    BitArray keyLeftPart = keyParts[0];
                    BitArray keyRightPart = keyParts[1];
                    // krok 6
                    keyLeftPart = ShiftLeft(keyLeftPart, Step6Shifts[i%16]); //Cn
                    keyRightPart = ShiftLeft(keyRightPart, Step6Shifts[i%16]); //Dn
                    // krok 7
                    key[j-1] = PermutedChoice2(SumBlocks(keyLeftPart, keyRightPart));
                    // krok 8
                    BitArray rightExtend = ExtendTablePermutation(rightPart);
                    // krok 9
                    BitArray afterXOR = rightExtend.Xor(key[j-1]);
                    // krok 10
                    BitArray[] bit6Parts = DivideBySixBits(afterXOR);
                    // krok 12
                    BitArray[] bit6Parts4Bits = Step12Function(bit6Parts);
                    // krok 13
                    BitArray mergedArray = MergeArray(bit6Parts4Bits);
                    // krok 14
                    BitArray permutedMergedArray = Step14(mergedArray);
                    // krok 15
                    BitArray rightPartPrevious = rightPart;
                    rightPart = permutedMergedArray.Xor(leftPart);
                    // krok 16
                    leftPart = rightPartPrevious;
                }
                // krok 17,18
                bits[i] = InverseInitialPermutation(SumBlocks(rightPart, leftPart));
            }

            bytes = BitArrayToByteArray(bits);
            return DecomplementBytes(bytes);
        }

        // Dopełnianie ISO 7816-4
        private byte[] ComplementBytes(byte[] bytes)
        {
            int difference = 8 - bytes.Length % 8;
            if (difference == 8)
                return bytes;

            byte[] newBytes = new byte[bytes.Length + difference];

            for (int i = 0; i < bytes.Length; i++)
                newBytes[i] = bytes[i];

            for (int i = bytes.Length; i < newBytes.Length; i++)
            {
                if (i == bytes.Length)
                    newBytes[i] = 0x80;
                else
                    newBytes[i] = 0x00;
            }

            return newBytes;
        }

        // Usunięcie dopełniania ISO 7816-4
        private byte[] DecomplementBytes(byte[] bytes)
        {
            for(int i = bytes.Length - 8; i < bytes.Length; i++)
            {
                if(bytes[i] == 0x80)
                {
                    bool isSequence = true;
                    for(int j = i + 1; j < bytes.Length; j++)
                    {
                        if(bytes[j] != 0x00)
                        {
                            isSequence = false;
                            break;
                        }
                    }

                    if(isSequence)
                    {
                        byte[] result = new byte[i];
                        Array.Copy(bytes, result, i);
                        return result;
                    }
                }
            }

            return bytes;
        }

        // Tworzy tablicę dwuwymiarową o 8 kolumnach
        private BitArray[] CreateTwoDimensionArray(BitArray bits)
        {
            int rowCount = bits.Length / 64;
            int columnCount = 64;
            BitArray[] result = new BitArray[rowCount];

            for (int i = 0; i < rowCount; i++)
            {
                result[i] = new BitArray(columnCount);
                for (int j = 0; j < columnCount; j++)
                    result[i][j] = bits[columnCount * i + j];
            }

            return result;
        }

        // Tworzy tablicę dwuwymiarową o 2 wierszach
        private BitArray[] DivideBitsInHalf(BitArray bits)
        {
            int rowCount = bits.Length / 2;
            int columnCount = 2;
            BitArray[] result = new BitArray[columnCount];

            for (int i = 0; i < columnCount; i++)
            {
                result[i] = new BitArray(rowCount);
                for (int j = 0; j < rowCount; j++)
                    result[i][j] = bits[rowCount * i + j];
            }

            return result;
        }

        // INPUT / OUTPUT : 8 bytes
        private BitArray InitialPermutation(BitArray block)
        {
            int[] permutationTable = {
                 58, 50, 42, 34, 26, 18, 10, 2 ,
                 60, 52, 44, 36, 28, 20, 12, 4 ,
                 62, 54, 46, 38, 30, 22, 14, 6 ,
                 64, 56, 48, 40, 32, 24, 16, 8 ,
                 57, 49, 41, 33, 25, 17,  9, 1 ,
                 59, 51, 43, 35, 27, 19, 11, 3 ,
                 61, 53, 45, 37, 29, 21, 13, 5 ,
                 63, 55, 47, 39, 31, 23, 15, 7
            };

            BitArray inputBlock = new BitArray(block);
            BitArray outputBlock = new BitArray(inputBlock.Length);

            for (int i = 0; i < outputBlock.Length; i++)
                outputBlock[i] = inputBlock[permutationTable[i] - 1];

            return outputBlock;
        }

        // INPUT 8 bytes
        // OUTPUT 56 bits = 7 bytes
        private BitArray PermutedChoice(BitArray block)
        {
            int[] permutationTable =
            {
                57, 49, 41, 33, 25, 17,  9,
                 1, 58, 50, 42, 34, 26, 18,
                10,  2, 59, 51, 43, 35, 27,
                19, 11,  3, 60, 52, 44, 36,
                63, 55, 47, 39, 31, 23, 15,
                 7, 62, 54, 46, 38, 30, 22,
                14,  6, 61, 53, 45, 37, 29,
                21, 13,  5, 28, 20, 12,  4
            };

            BitArray inputBlock = new BitArray(block);
            BitArray outputBlock = new BitArray(56);

            for (int i = 0; i < permutationTable.Length; i++)
                outputBlock[i] = inputBlock[permutationTable[i] - 1];

            return outputBlock;
        }

        // INPUT 7 bytes
        // OUTPUT 48 bits = 6 bytes
        private BitArray PermutedChoice2(BitArray block)
        {
            int[] permutationTable =
            {
                14, 17, 11, 24,  1,  5,
                 3, 28, 15,  6, 21, 10,
                23, 19, 12,  4, 26,  8,
                16,  7, 27, 20, 13,  2,
                41, 52, 31, 37, 47, 55,
                30, 40, 51, 45, 33, 48,
                44, 49, 39, 56, 34, 53,
                46, 42, 50, 36, 29, 32
            };

            BitArray inputBlock = new BitArray(block);
            BitArray outputBlock = new BitArray(48);

            for (int i = 0; i < permutationTable.Length; i++)
                outputBlock[i] = inputBlock[permutationTable[i] - 1];

            return outputBlock;
        }

        // 32bit block to 48bit
        private BitArray ExtendTablePermutation(BitArray block)
        {
            int[] permutationTable = {
                 32,  1,  2,  3,  4,  5,
                  4,  5,  6,  7,  8,  9,
                  8,  9, 10, 11, 12, 13,
                 12, 13, 14, 15, 16, 17,
                 16, 17, 18, 19, 20, 21,
                 20, 21, 22, 23, 24, 25,
                 24, 25, 26, 27, 28, 29,
                 28, 29, 30, 31, 32,  1,
            };

            BitArray inputBlock = new BitArray(block);
            BitArray outputBlock = new BitArray(48);

            for (int i = 0; i < outputBlock.Length; i++)
                outputBlock[i] = inputBlock[permutationTable[i] - 1];

            return outputBlock;
        }

        private BitArray ShiftLeft(BitArray block, int x = 1)
        {
            BitArray inputBlock = new BitArray(block);

            int i = 0;
            for (i = 0; i < inputBlock.Length; i++)
                inputBlock[i] = inputBlock[(i + x) % inputBlock.Length];

            //for (; i < inputBlock.Length; i++)
            //    inputBlock[i] = inputBlock[;

            return inputBlock;
        }

        // łączy bloki w jeden
        private BitArray SumBlocks(BitArray block1, BitArray block2)
        {
            BitArray sumBlock = new BitArray(block1.Length + block2.Length);
            for (int i = 0; i < sumBlock.Length; i++)
                if (i < block1.Length)
                    sumBlock[i] = block1[i];
                else
                    sumBlock[i] = block2[i - block1.Length];

            return sumBlock;
        }

        // dzielimy na 6 bitowe blocki
        private BitArray[] DivideBySixBits(BitArray bits)
        {
            int rowCount = bits.Length / 6;
            int columnCount = 6;
            BitArray[] result = new BitArray[rowCount];

            for (int i = 0; i < rowCount; i++)
            {
                result[i] = new BitArray(columnCount);
                for (int j = 0; j < columnCount; j++)
                    result[i][j] = bits[columnCount * i + j];
            }

            return result;
        }

        private BitArray[] Step12Function(BitArray[] bitArray)
        {
            BitArray[] result = new BitArray[bitArray.Length]; //8 ciągów po 4 bity
            BitArray helper;
            string columnBinary = String.Empty; // [5][0] -> wiersz, [4][3][2][1] -> kolumna
            string rowBinary = String.Empty;
            string valueInArrayBinary = String.Empty;
            int column, row, valueInArray;
            bool[] bool4BitNumber = new bool[4];
            for (int i = 0; i < bitArray.Length; i++) //pętla po 8 blokach składających się z 6 bitów
            {
                helper = bitArray[i]; //bitArray[i] - tablica składająca się z 6 bitów
                for (int j = 5; j >= 0; j--) //pętla po jednym bloku 6 bitowym
                {
                    if (j == 0 || j == 5)
                    {
                        rowBinary += helper[j] ? "1" : "0";
                    }
                    else
                    {
                        columnBinary += helper[j] ? "1" : "0";
                    }
                }
                column = Convert.ToInt32(columnBinary, 2);
                row = Convert.ToInt32(rowBinary, 2);
                valueInArray = Step12Arrays[i][row * 16 + column];
                valueInArrayBinary = Convert.ToString(valueInArray, 2);
                //System.Diagnostics.Debug.WriteLine(valueInArrayBinary);

                result[i] = new BitArray(4);
                int stringLength = valueInArrayBinary.Length;
                for (int k = 0; k < 4 - stringLength; k++)
                {
                    valueInArrayBinary = "0" + valueInArrayBinary;
                }

                for (int j = 0; j < 4; j++)
                {
                    result[i][j] = valueInArrayBinary[3 - j] == '0' ? false : true;
                }

                //if(valueInArrayBinary.Length < 4)
                //{
                //    for(int j=0;)
                //}
                //arrayTest[0] = true;
                //arrayTest[1] = true;
                //arrayTest[2] = true;
                //arrayTest[3] = true;

                //result[i] = new BitArray(arrayTest);
                //System.Diagnostics.Debug.WriteLine(result[i].Length);
                //System.Diagnostics.Debug.WriteLine(result[i][0]);
                //System.Diagnostics.Debug.WriteLine(result[i][1]);
                //System.Diagnostics.Debug.WriteLine(result[i][2]);
                //System.Diagnostics.Debug.WriteLine(result[i][3]);

                //System.Diagnostics.Debug.WriteLine("Wiersz: " + row + " Kolumna: " + column + " value from array: " + valueInArray);

                columnBinary = String.Empty;
                rowBinary = String.Empty;
            }
            return result;
        }

        private BitArray MergeArray(BitArray[] bitArray)
        {

            BitArray result = new BitArray(32);
            for (int i = 0; i < 32; i++)
            {
                result[i] = bitArray[i % 8][i % 4];
            }
            return result;
        }

        private BitArray Step14(BitArray block)
        {
            int[] permutationTable =
            {
                16, 7, 20, 21,
                29, 12, 28, 17,
                1, 15, 23, 26,
                5, 18, 31, 10,
                2, 8, 24, 14,
                32, 27, 3, 9,
                19, 13, 30, 6,
                22, 11, 4, 25,
            };

            BitArray inputBlock = new BitArray(block);
            BitArray outputBlock = new BitArray(32);

            for (int i = 0; i < permutationTable.Length; i++)
                outputBlock[i] = inputBlock[permutationTable[i] - 1];

            return outputBlock;
        }

        private BitArray InverseInitialPermutation(BitArray block)
        {
            int[] permutationTable = {
                 40, 8, 48, 16, 56, 24, 64, 32,
                 39, 7, 47, 15, 55, 23, 63, 31,
                 38, 6, 46, 14, 54, 22, 62, 30,
                 37, 5, 45, 13, 53, 21, 61, 29,
                 36, 4, 44, 12, 52, 20, 60, 28,
                 35, 3, 43, 11, 51, 19, 59, 27,
                 34, 2, 42, 10, 50, 18, 58, 26,
                 33, 1, 41, 9, 49, 17, 57, 25
            };

            BitArray inputBlock = new BitArray(block);
            BitArray outputBlock = new BitArray(inputBlock.Length);

            for (int i = 0; i < outputBlock.Length; i++)
                outputBlock[i] = inputBlock[permutationTable[i] - 1];

            return outputBlock;
        }

        private byte ConvertToByte(BitArray bits)
        {
            if (bits.Count != 8)
            {
                throw new ArgumentException("bits");
            }
            byte[] bytes = new byte[1];
            bits.CopyTo(bytes, 0);
            return bytes[0];
        }

        public static byte[] BitArrayToByteArray(BitArray[] bits)
        {
            int total = bits.Length * bits[0].Length;
            byte[] bytes = new byte[total / 8];

            for(int i = 0; i < bits.Length; i++)
            {
                bits[i].CopyTo(bytes, 8*i);
            }

            return bytes;
       }
    }
}
