﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cryptography.Algorithms
{
    public class DisplacementMatrixNumbers : IAlgorithm
    {
        public bool AreParametersValid(object parameters)
        {
            string[] keys = parameters.ToString().Split('-');
            uint maks = 1;
            uint n;
            bool isNumeric;
            foreach (var sub in keys){
                isNumeric = UInt32.TryParse(sub, out n);
                if(!isNumeric || n < 1){
                    return false;
                }

                if (n > maks)
                    maks = n;
            }

            maks = maks - 1;
            while(maks > 0)
            {
                if (!keys.Contains(maks.ToString()))
                    return false;

                maks--;
            }


            return true;
        }

        public string Decode(string content, object parameters)
        {
            string[] key = parameters.ToString().Split('-');
            content = content.ToUpper();

            int columns = key.Length;
            int rows = content.Length / columns + 1;

            char[,] matrix = new char[rows, columns];

            int columnsPassed = 0;
            int lettersPassed = 0;
            int emptySpacesInLastRow = columns * rows % content.Length;

            for(int j = columns - emptySpacesInLastRow; j < columns; j++){
                matrix[rows-1, j] = ' ';
            }
                
            for(int i = 0; i < rows ;i++){
                 while(columnsPassed < columns){

                    if(lettersPassed >= content.Length)
                        break;

                    if(matrix[i, int.Parse(key[columnsPassed]) - 1] != ' ')
                        matrix[i, int.Parse(key[columnsPassed++]) - 1] = content[lettersPassed++];
                    else
                        columnsPassed++;
                }
                columnsPassed = 0;
            }

            string result = "";
            for(int i = 0; i < rows; i++){
                for(int j = 0; j < columns; j++)
                {
                    if(!Char.IsWhiteSpace(matrix[i,j]) && matrix[i,j] != '\x0000')
                        result+= matrix[i,j];
                }
            }
            return result;
        }

        public string Encode(string content, object parameters)
        {
            string[] key = parameters.ToString().Split('-');
            content = content.ToUpper();

            int columns = key.Length;
            int rows = content.Length / columns + 1;

            char[,] matrix = new char[rows,columns];

            int r = 0, c = 0;

            for(int i = 0; i < content.Length; i++)
            {
                matrix[r, c++] = content[i];
                if (c >= columns)
                {
                    r++;
                    c = 0;
                }
            }

            string result = "";
            int columnsPassed = 0;
            for(int i = 0; i < rows; i++){
                while(columnsPassed<columns){
                    if (!Char.IsWhiteSpace(matrix[i, int.Parse(key[columnsPassed]) - 1]) && matrix[i, int.Parse(key[columnsPassed]) - 1] != '\x0000')
                        result += matrix[i, int.Parse(key[columnsPassed]) - 1];
                    columnsPassed++;
                }
                columnsPassed = 0;
            }

            return result;
        }
    }

}