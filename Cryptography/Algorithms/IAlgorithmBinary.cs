﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cryptography.Algorithms
{
    public interface IAlgorithmBinary
    {
        byte EncodeByte(byte content, object parameters);
        byte DecodeByte(byte content, object parameters);
    }
}
