﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cryptography.Algorithms
{
    public class CaesarCipher : IAlgorithm
    {
        private const int N = 26; // liczba znaków w angielskim alfabecie

        public bool AreParametersValid(object parameters)
        {
            return UInt32.TryParse(parameters.ToString(), out uint result);
        }

        public string Decode(string content, object parameters)
        {
            content = GetEnglishCapitalLetters(content, "Wejście");
            string result = "";
            int k = int.Parse(parameters.ToString()); // klucz

            foreach (char c in content)
            {
                int mod = (c - 'A' + N - k) % N;
                if (mod < 0)
                    mod += N;

                result += (char)(mod + 'A');
            }

            return result;
        }

        public string Encode(string content, object parameters)
        {
            content = GetEnglishCapitalLetters(content, "Wejście");
            string result = "";
            int k = int.Parse(parameters.ToString()); // klucz

            foreach (char a in content)
                result += (char)((a - 'A' + k) % N + 'A');

            return result;
        }

        public static string GetEnglishCapitalLetters(string letters, string source)
        {
            letters = letters.Replace(" ", "");
            letters = letters.Replace("Ą", "A").Replace("ą", "A");
            letters = letters.Replace("Ć", "C").Replace("ć", "C");
            letters = letters.Replace("Ę", "E").Replace("ę", "E");
            letters = letters.Replace("Ł", "L").Replace("ł", "L");
            letters = letters.Replace("Ń", "N").Replace("ń", "N");
            letters = letters.Replace("Ó", "O").Replace("ó", "O");
            letters = letters.Replace("Ś", "S").Replace("ś", "S");
            letters = letters.Replace("Ź", "Z").Replace("ź", "Z");
            letters = letters.Replace("Ż", "Z").Replace("ż", "Z");
            letters = letters.ToUpper();

            foreach (char c in letters)
            {
                if (c < 'A' || c > 'Z')
                    throw new InvalidInputException($"{source} '{letters}' zawiera niedozwolone znaki");
            }

            return letters;
        }
    }
}
