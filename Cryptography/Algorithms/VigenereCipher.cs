﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Cryptography.Algorithms
{
    public class VigenereCipher : IAlgorithm
    {
        public bool AreParametersValid(object parameters)
        {
            string key = (parameters as String);
            if (key.Length == 0)
                return false;

            try
            {
                key = CaesarCipher.GetEnglishCapitalLetters(key, "Klucz");
            }
            catch (InvalidInputException)
            {
                return false;
            }

            return true;
        }

        public string Decode(string content, object parameters)
        {
            string key = (parameters as String);
            key = CaesarCipher.GetEnglishCapitalLetters(key, "Klucz");
            content = CaesarCipher.GetEnglishCapitalLetters(content, "Wejście");

            int j = 0;
            while (key.Length < content.Length)
            {
                key += key[j++];
                if (key.Length == j) j = 0;
            }
            string result = string.Empty;
            for (int i = 0; i < content.Length; i++)
            {
                result += (char)(((content[i] - 'A') - (key[i % key.Length] - 'A') + 26) % 26 + 'A');
            }
            return result;
        }

        public string Encode(string content, object parameters)
        {
            string key = (parameters as String);
            key = CaesarCipher.GetEnglishCapitalLetters(key, "Klucz");
            content = CaesarCipher.GetEnglishCapitalLetters(content, "Wejście");

            int j = 0;
            while (key.Length < content.Length)
            {
                key += key[j++];
                if (key.Length == j) j = 0;
            }
            string result = string.Empty;
            for (int i = 0; i < content.Length; i++)
            {
                result += (char)(((content[i] - 'A') + (key[i % key.Length] - 'A')) % 26 + 'A');
            }
            return result;
        }
    }
}
