﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cryptography.Algorithms
{
    public interface IAlgorithm
    {
        bool AreParametersValid(object parameters);
        string Encode(string content, object parameters);
        string Decode(string content, object parameters);
    }
}
