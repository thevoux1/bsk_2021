﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cryptography.Algorithms
{
    public class RailFence : IAlgorithm
    {
        public bool AreParametersValid(object parameters)
        {
            uint value;
            bool result = UInt32.TryParse(parameters.ToString(), out value);

            if (!result || value == 0)
                return false;

            return true;
        }

        public string Decode(string content, object parameters)
        {
            uint n = UInt32.Parse(parameters.ToString());
            char[,] table = new char[n, content.Length];

            int verticalIndex = 0;
            bool isRising = true;
            for (int horizontalIndex = 0; horizontalIndex < content.Length; horizontalIndex++)
            {
                table[verticalIndex, horizontalIndex] = (char)8;

                if (verticalIndex == n - 1)
                    isRising = false;
                else if (verticalIndex == 0)
                    isRising = true;

                if (isRising)
                    verticalIndex++;
                else
                    verticalIndex--;
            }

            int wordIndex = 0;
            for (int i = 0; i < n && wordIndex < content.Length; i++)
            {
                for (int j = 0; j < content.Length && wordIndex < content.Length; j++)
                {
                    if (table[i, j] == (char)8)
                        table[i, j] = content[wordIndex++];
                }
            }

            string result = "";
            verticalIndex = 0;
            isRising = true;
            for (int horizontalIndex = 0; horizontalIndex < content.Length; horizontalIndex++)
            {
                result += table[verticalIndex, horizontalIndex];

                if (verticalIndex == n - 1)
                    isRising = false;
                else if (verticalIndex == 0)
                    isRising = true;

                if (isRising)
                    verticalIndex++;
                else
                    verticalIndex--;
            }

            return result;
        }

        public string Encode(string content, object parameters)
        {
            uint n = UInt32.Parse(parameters.ToString());
            char[,] table = new char[n, content.Length];
            /////
            /// liczba wierszy, liczba kolumn
            ////

            int verticalIndex = 0;
            bool isRising = true;
            for(int horizontalIndex = 0; horizontalIndex < content.Length; horizontalIndex++)
            {
                table[verticalIndex, horizontalIndex] = content[horizontalIndex];

                if (verticalIndex == n - 1)
                    isRising = false;
                else if (verticalIndex == 0)
                    isRising = true;

                if (isRising)
                    verticalIndex++;
                else
                    verticalIndex--;
            }

            string result = "";
            for(int i=0; i < n; i++)
            {
                for (int j = 0; j < content.Length; j++)
                {
                    if (table[i, j] != '\0')
                        result += table[i, j];
                }
            }

            return result;
        }
    }
}
